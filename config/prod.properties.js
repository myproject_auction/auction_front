

const prodProperties = {
  vendors: ['src/vendor.js'],

  outputDir: '/common/mes-starter',  // mes-starter를 appName으로 수정
  publicUrl: '/common/mes-starter',      // 배포 될 웹서버의 URL prefix
  apiUrl: '/mes-starter-service',        // mes-starter-service를 serviceName으로 수정
};


module.exports = prodProperties;
