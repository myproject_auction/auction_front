import { userStore } from './user';
import { itemStore } from './item';

const store = {
    userStore,
    itemStore
}

export default store;