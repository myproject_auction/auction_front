
import React, { PureComponent } from 'react';
import { Form, Button } from 'semantic-ui-react';

class ItemEditFormView extends PureComponent {
    //
    render() {
        //
        const {
            item,
            registerItem,
            clearItem,
            setItemProp,
        } = this.props;

        return (
            <Form>
                <Form.Group widths='equal'>
                    <Form.Input 
                        fluid label='Name' 
                        placeholder='Name'
                        value={item && item.name || ''}
                        onChange={(e) => setItemProp('name', e.target.value)}
                        readOnly={item && item.id ? true : false}
                    />
                </Form.Group>
                <Form.Group widths='equal'>
                    <Form.TextArea 
                        label='Description' 
                        placeholder='Description' 
                        value={item && item.description || ''}
                        onChange={(e) => setItemProp('description', e.target.value)}
                    />
                </Form.Group>
                <Form.Group widths='equal'>
                    <Form.Input 
                        fluid label='Start Price' 
                        placeholder='Start Price' 
                        value={item && item.startPrice || ''}
                        onChange={(e) => setItemProp('startPrice', e.target.value)}
                    />
                    <Form.Input 
                        fluid label='Price Step' 
                        placeholder='Price Step' 
                        value={item && item.priceStep || ''}
                        onChange={(e) => setItemProp('priceStep', e.target.value)}
                    />
                </Form.Group>
                <Button primary onClick={() => registerItem(item)}>Save</Button>
                <Button secondary onClick={clearItem}>Cancel</Button>
            </Form>
        )
    }
}

export default ItemEditFormView;