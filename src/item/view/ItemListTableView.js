
import React, { PureComponent } from 'react';
import { Table } from 'semantic-ui-react';

class ItemListTableView extends PureComponent {
    //
    render() {
        //
        const {
            items,
        } = this.props;
        return (
            <Table celled>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Name</Table.HeaderCell>
                        <Table.HeaderCell>Start Price</Table.HeaderCell>
                        <Table.HeaderCell>Price Step</Table.HeaderCell>
                        <Table.HeaderCell>Current Price</Table.HeaderCell>
                        <Table.HeaderCell>Seller</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {
                        Array.isArray(items) && items.length ?
                        items.map((item) => {
                            return (
                                <Table.Row >
                        <Table.Cell style={{cursor: 'pointer'}}
                        >
                            {item.name}
                        </Table.Cell>
                        <Table.Cell>{item.startPrice}</Table.Cell>
                        <Table.Cell>{item.priceStep}</Table.Cell>
                        <Table.Cell>{item.currentPrice}</Table.Cell>
                        <Table.Cell>{item.seller}</Table.Cell>
                    </Table.Row>

                            );
                        })
                    :
                    <Table.Row>
                        <Table.Cell>No Items</Table.Cell>
                    </Table.Row>
                    }
                </Table.Body>
            </Table>
        );
    }
}

export default ItemListTableView;