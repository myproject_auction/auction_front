
import React, { PureComponent } from 'react';
import { Container, Header, Button, Divider } from 'semantic-ui-react';

import ItemListTableView from './ItemListTableView';
import ItemEditFormView from './ItemEditFormView';

class ItemListView extends PureComponent {
    //
    render() {
        //
        const {
            user,
            item, 
            items, 
            clearItem, 
            setNewItem, 
            setItemProp,
            registerItem,
            findItem,
            findItemBySeller,
        } = this.props;

        return (
            user !== null &&
            <Container style={{marginTop: 50}}>
                <Header as='h2'>
                     {user.name}'s Item List
                     <Button 
                        floated='right' primary
                        onClick={setNewItem}>
                            New
                    </Button>
                </Header>
                <Divider/>
                {
                    item &&
                    <ItemEditFormView
                        item={item}
                        registerItem={registerItem}
                        clearItem={clearItem}
                        setItemProp={setItemProp}
                    />
                    || ''
                }
                
                { item && <Divider/> || '' }
                <ItemListTableView
                    items={items}
                    findItem={findItem}
                    findItemBySeller={findItemBySeller}
                />
            </Container>
        );
    }
}

export default ItemListView;