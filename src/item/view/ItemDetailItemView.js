
import React, { PureComponent } from 'react';
import { Item } from 'semantic-ui-react';

class ItemDetailItemView extends PureComponent {
    //
    render() {
        //
        const {
        } = this.props;

        return (  
            <Item.Group>
                <Item>
                    <Item.Content>
                        <Item.Header as='a'></Item.Header>
                        <Item.Meta>Description</Item.Meta>
                        <Item.Description>
                        </Item.Description>
                        <Item.Extra>Start Price : </Item.Extra>
                        <Item.Extra>Price Step : </Item.Extra>
                        <Item.Extra>Current Price : </Item.Extra>
                    </Item.Content>
                </Item>
            </Item.Group>
        )
    }
}

export default ItemDetailItemView;