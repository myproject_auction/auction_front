import React from 'react';
import { observer, inject } from 'mobx-react';
import ItemListView from '../view/ItemListView';
import queryString from 'query-string';

@inject('userStore', 'itemStore')
@observer
class ItemListContainer extends React.Component {

    componentDidMount() {

        const { sellerId } = queryString.parse(this.props.location.search);

        this.props.userStore.findUser(sellerId);
        this.props.itemStore.findItemsBySeller(sellerId);

    }

    render() {

        const { userStore, itemStore } = this.props;

        return (
            <ItemListView 
                user={userStore.getUser}
                item={itemStore.getItem}
                items={itemStore.getITems}
                clearItem={itemStore.clearItem}
                setNewItem={itemStore.setNewItem}
                setItemProp={itemStore.setItemProp}
                registerItem={itemStore.registerItem}
                findItem={itemStore.findItem}
                findItemsBySeller={itemStore.findItemsBySeller}
            />
        );
    }
}

export default ItemListContainer;