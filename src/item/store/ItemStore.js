import { observable, computed, action, toJS } from 'mobx';
import ItemRepository from '../repository/ItemRepository';

class ItemStore {

    @observable
    item = null;

    @observable
    items = [];

    @computed
    get getItem() {
        return toJS(this.item);
    }

    @computed
    get getItems() {
        return toJS(this.items);
    }

    @action
    registerItem = (item) => {
        console.log('register', item);
        ItemRepository.registerItem(item);
    }

    @action
    findItem = (itemId) => {
        ItemRepository.findItem(itemId)
        .then(item => this.item = item);
    }

    @action
    findItemsBySeller = (sellerId) => {
        ItemRepository.findItemsBySeller(sellerId)
        .then(items => this.items = items);
    }

    @action
    clearItem = () => {
        this.item = null;
    }

    @action
    setNewItem = () => {
        this.item = {};
    }

    @action
    setItemProp = (name, value) => {

        const item = { ...this.item, [name]: value }; 
        this.item = item;

    }
}

export default ItemStore;
export const itemStore = new ItemStore();