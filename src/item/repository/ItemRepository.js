import axios from 'axios';

class ItemRepository {
    registerItem = (item) => {
        return axios.post(`http://localhost:8080/item`, item)
        .then(response => response && response.data || null)
        .catch(err => console.log(err));
    };

    findItem = (itemId) => {
        return axios.get(`http://localhost:8080/item/${itemId}`)
        .then(response => response && response.data || null)
        .catch(err => console.log(err));
    }

    findItemsBySeller = (sellerId) => {
        return axios.get(`http://localhost:8080/item/seller/${sellerId}`)
        .then(response => response && response.data || null)
        .catch(err => console.log(err));
    }
}

export default new ItemRepository();