import Routes from './Routes';
import React, { Component } from 'react';
import store from './store';
import { Provider } from 'mobx-react';

class App extends Component {
	render(){
		return (
			<Provider
				{...store}
			>
				<Routes/>
			</Provider>
		);
	}
}
export default App;
