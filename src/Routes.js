import React, { } from 'react';
import { BrowserRouter, Switch, Redirect, Route } from 'react-router-dom';
import { UserList } from './user';
import { ItemList } from './item';

function Routes() {
    return (
        <BrowserRouter basename="/">
            <Switch>
                <Redirect exact from="/" to="/front/user"/>
                <Route path="/front" component={({match}) =>
                    <React.Fragment>
                        <Route exact path={`${match.path}/user`} component={UserList} />
                        <Route exact path={`${match.path}/item`} component={ItemList} />
                    </React.Fragment>
                }/>
            </Switch>
        </BrowserRouter>
    );
}

export default Routes;
