
import React, { PureComponent } from 'react';
import { Container, Header, Divider } from 'semantic-ui-react';

import BidListByBidderListView from './BidListByBidderListView';

class BidListByBidderView extends PureComponent {
    //
    render() {
        //
        const {
        } = this.props;

        return (
            <Container style={{marginTop: 50}}>

                <Header as='h2'>
                </Header>
                <Divider/>
                <BidListByBidderListView />
            </Container>
        );
    }
}

export default BidListByBidderView;