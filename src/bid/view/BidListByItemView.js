
import React, { PureComponent } from 'react';
import { Container, Header, Divider } from 'semantic-ui-react';

import BidEditFormView from './BidEditFormView';
import BidListByItemListView from './BidListByItemListView';
import { ItemDetailContainer } from '../../item/index';

class BidListByItemView extends PureComponent {
    //
    constructor(props) {
        super(props);
    }
    render() {
        //
        const {
        } = this.props;

        return (
            <Container style={{marginTop: 50}}>
                <Divider/>
                <ItemDetailContainer 
                />
                <Divider/>
                <BidEditFormView 
                />
                <Divider/>
                <BidListByItemListView/>
            </Container>
        );
    }
}

export default BidListByItemView;