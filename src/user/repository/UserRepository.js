import axios from 'axios';

class UserRepository {

    registerUser = (user) => {
        return axios.post(`http://localhost:8080/user`, user)
        .then(response => response && response.data || null)
        .catch(err => console.log(err));
    };

    findAllUsers = () => {
        return axios.get(`http://localhost:8080/user`)
        .then(response => response && response.data || null)
        .catch(err => console.log(err));
    }

    findUser = (userId) => {
        return axios.get(`http://localhost:8080/user/${userId}`)
        .then(response => response && response.data || null)
        .catch(err =>  console.log(err));
    }
    
}

export default new UserRepository();