import { userStore } from './store/UserStore';
import UserList from '../user/container/UserListContainer'

export default {
    userStore,
    UserList,
}

export {
    userStore,
    UserList,
}