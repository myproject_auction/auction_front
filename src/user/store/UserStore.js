import { observable, computed, action, toJS } from 'mobx';
import UserRepository from '../repository/UserRepository';
import UserModel from '../model/UserModel';

class UserStore {
    
    @observable
    user = null;

    @observable
    users = [];

    @computed
    get getUser() {
        return toJS(this.user);
    }

    @computed
    get getUsers() {
        return toJS(this.users);
    }

    @action
    registerUser = (user) => {
        UserRepository.registerUser(user);
    }

    @action
    findUser = (userId) => {
        UserRepository.findUser(userId)
        .then(user => this.user = new UserModel(user));
    }

    @action
    findAllUsers = () => {
        UserRepository.findAllUsers()
        .then(users => this.users = users.map(user => new UserModel(user)));
    }

    @action
    clearUser = () => {
        this.user = null;
    }

    @action
    setNewUser = () => {
        this.user = {};
    }

    @action
    setUserProp = (name, value) => {
        const user = { ...this.user, [name]: value };
        this.user = user;
    }
}

export default UserStore;
export const userStore = new UserStore(); 