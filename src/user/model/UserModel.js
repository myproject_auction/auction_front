
class UserModel {
    
    constructor(user = {}) {
        Object.assign(this, user) ;

        if(user.roles && user.roles.length) {
            let roleString = '';
            
            user.roles.forEach((role, index) => {
                if(index > 0) roleString = `${roleString},`;
                roleString = `${roleString}${role}`;
            });

            this.roleString = roleString;
        }
    }
}

export default UserModel;