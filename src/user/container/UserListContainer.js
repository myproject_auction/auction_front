import React from 'react';
import { observer, inject } from 'mobx-react';
import UserListView from '../view/UserListView';

@inject('userStore')
@observer
class UserListContainer extends React.Component {
    
    componentDidMount() {
        const { userStore } = this.props;
        userStore.findAllUsers();
    }

    routeToItem = (user) => {
        this.props.history.push(`/front/item?sellerId=${user.id}`);
    }

    onSaveUser = (user) => {
        Promise.resolve()
        .then(() => this.props.userStore.registerUser(user))
        .then(this.props.userStore.findAllUsers);
    }

    render() {

        const { userStore } = this.props;

        return (
            <UserListView 
                user={userStore.getUser}
                users={userStore.getUsers}
                clearUser={userStore.clearUser}
                setNewUser={userStore.setNewUser}
                setUserProp={userStore.setUserProp}
                onSaveUser={this.onSaveUser}
                routeToItem={this.routeToItem}
            />
        );
    }
}

export default UserListContainer;