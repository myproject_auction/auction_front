import React, { PureComponent } from 'react';
import { Form, Button } from 'semantic-ui-react';

class UserEditFormView extends PureComponent {
    //
    render() {
        //
        const {
            user,
            setUserProp,
            clearUser,
            onSaveUser,
        } = this.props;

        let roles = user && user.roles || [];
        const onCheckRole = (e, data) => {
            if(data.checked) roles.push(data.value);
            else roles = roles.filter(role => role !== data.value);
            setUserProp('roles', roles);
        }

        return (
            <Form>
                <Form.Group widths='equal'>
                    <Form.Input 
                        fluid label='Name' 
                        placeholder='Name'
                        value={user && user.name || ''}
                        onChange={(e) => setUserProp('name', e.target.value)}
                        readOnly={user && user.id ? true : false}
                    />
                </Form.Group>
                <Form.Group inline>
                    <label>Role</label>
                    <Form.Checkbox
                        label='Seller'
                        value='Seller'
                        checked={roles.includes('Seller')}
                        onChange={onCheckRole}
                        readOnly={user && user.id ? true: false}
                    />
                    <Form.Checkbox
                        label='Bidder'
                        value='Bidder'
                        checked={roles.includes('Bidder')}
                        onChange={onCheckRole}
                        readOnly={user && user.id ? true: false}
                    />
                </Form.Group>
                <Button primary onClick={() => onSaveUser(user)}>Save</Button>
                <Button secondary onClick={clearUser} >Cancel</Button>
            </Form>
        )
    }
}

export default UserEditFormView;