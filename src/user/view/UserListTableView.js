
import React, { PureComponent } from 'react';
import { Table, Button } from 'semantic-ui-react';

class UserListTableView extends PureComponent {
    //
    render() {
        //
        const {
            users,
            routeToItem,
        } = this.props;

        return (
            <Table celled>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Name</Table.HeaderCell>
                        <Table.HeaderCell>Role</Table.HeaderCell>
                        <Table.HeaderCell>Item</Table.HeaderCell>
                        <Table.HeaderCell>Bid</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {
                        Array.isArray(users) && users.length ?
                        users.map((user) => {
                            return (
                                <Table.Row >
                                    <Table.Cell style={{ cursor: 'pointer' }}>
                                        {user.name}
                                    </Table.Cell>
                                    <Table.Cell>
                                        {user.roleString}
                                    </Table.Cell>
                                    <Table.Cell textAlign='center'>
                                        {
                                            user && user.roles && user.roles.length && user.roles.includes('Seller') ?
                                            <Button onClick={() => routeToItem(user)}>Move</Button>
                                            : ''
                                        }
                                    </Table.Cell>
                                    <Table.Cell textAlign='center'>
                                        {
                                            user && user.roles && user.roles.length && user.roles.includes('Bidder') ? 
                                            <Button onClick={() => {}}>Move</Button>
                                            : ''
                                        }
                                    </Table.Cell>
                                </Table.Row>
                            );
                        })
                        :
                        <Table.Row>
                            <Table.Cell>No Users</Table.Cell>
                        </Table.Row>
                    }
                </Table.Body>
            </Table>
        );
    }
}

export default UserListTableView;
