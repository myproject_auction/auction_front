
import React, { PureComponent } from 'react';
import { Container, Header, Button, Divider } from 'semantic-ui-react';

import UserListTableView from './UserListTableView';
import UserEditFormView from './UserEditFormView';

class UserListView extends PureComponent {
    //
    render() {
        //
        const { 
            user, 
            users, 
            clearUser, 
            setNewUser, 
            setUserProp,
            routeToItem,
            onSaveUser,
        } = this.props;

        return (
            <Container style={{ marginTop: 50 }}>
                <Header as='h2'>
                    User List
                    <Button 
                        floated='right' 
                        primary
                        onClick={setNewUser}>
                            New
                    </Button>
                </Header>
                <Divider/>
                {
                    user && 
                    <UserEditFormView
                        user={user}
                        clearUser={clearUser}
                        setUserProp={setUserProp}
                        onSaveUser={onSaveUser}
                    />
                    || ''
                }

                { user && <Divider /> || '' }
                <UserListTableView
                    users={users}
                    routeToItem={routeToItem}
                />
            </Container>
        );
    }
}

export default UserListView;